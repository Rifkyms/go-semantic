const requestData = (url, options, callback, fallout) => {
  $.ajax({
    type: options.method,
    url: url,
    data: options.params,
    success: function (data) {
      callback(data)
    },
    error: function (xhr, textStatus, err) {
      if (xhr.status === 408) {
        console.error({
          title: 'Failed',
          message: 'Request timed out'
        })
      }
      if (fallout) {
        fallout(xhr)
      } else {
        console.error({
          title: xhr.statusText,
          message: xhr.responseText
        })
      }
    }
  })
}
