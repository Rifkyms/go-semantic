package routes

import (
	"io/ioutil"
	"log"
	"net/http"

	"github.com/gofiber/fiber/v2"
)

func CommonRouter(app fiber.Router) {
	app.Get("/common", Common)
	app.Get("/getTable", Datatable)
}

func Datatable(c *fiber.Ctx) error {
	return c.Render("datatable/index", fiber.Map{})
}

func Common(c *fiber.Ctx) error {
	resp, err := http.Get("https://api.kawalcorona.com/indonesia")
	if err != nil {
		log.Fatalln(err)
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
	}
	sb := string(body)
	return c.JSON(fiber.Map{
		"status":  200,
		"message": "Success Loaded Data",
		"data":    sb,
	})
}
