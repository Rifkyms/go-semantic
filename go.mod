module dasar-golang

go 1.16

require (
	github.com/andybalholm/brotli v1.0.3 // indirect
	github.com/cosmtrek/air v1.27.3
	github.com/creack/pty v1.1.17 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
	github.com/fatih/color v1.13.0 // indirect
	github.com/fsnotify/fsnotify v1.5.1 // indirect
	github.com/gofiber/fiber/v2 v2.13.0
	github.com/gofiber/jwt/v2 v2.2.3 // indirect
	github.com/gofiber/template v1.6.12
	github.com/golang-jwt/jwt v3.2.1+incompatible // indirect
	github.com/howeyc/fsnotify v0.9.0 // indirect
	github.com/klauspost/compress v1.13.1 // indirect
	github.com/mattn/go-colorable v0.1.11 // indirect
	github.com/pelletier/go-toml v1.9.4 // indirect
	github.com/pilu/config v0.0.0-20131214182432-3eb99e6c0b9a // indirect
	github.com/pilu/fresh v0.0.0-20190826141211-0fa698148017 // indirect
	github.com/valyala/fasthttp v1.28.0 // indirect
	golang.org/x/sys v0.0.0-20211113001501-0c823b97ae02 // indirect
)
