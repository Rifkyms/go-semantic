package main

import (
	"dasar-golang/routes"
	"log"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/gofiber/template/pug"
)

func main() {
	engine := pug.New("./views", ".pug")
	app := fiber.New(fiber.Config{
		Views: engine,
	})
	app.Static("/", "./public")
	app.Use(logger.New())

	app.Get("/", func(c *fiber.Ctx) error {
		return c.Render("index", fiber.Map{})
	})

	app.Get("/layout", func(c *fiber.Ctx) error {
		return c.Render("main/main", fiber.Map{})
	})

	routes.CommonRouter(app)
	log.Fatal(app.Listen(":4000"))
}
